<?php
/**
 * @file
 * Documentation for Facebook Scrape API.
 */

/**
 * Define custom entity types to enable for Facebook Scrape.
 */
function hook_fb_scrape_entity_types() {
  return array(
    'my_entity' => t('My entity'),
  );
}
