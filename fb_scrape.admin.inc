<?php
/**
 * @file
 * Administration functions for Facebook Scrape.
 */

/**
 * Menu callback: settings form.
 */
function fb_scrape_settings_form() {
  $fb_app_options = array();
  $form['fb_scrape_app'] = array(
    '#type' => 'fieldset',
    '#title' => t('Facebook App mode'),
    '#collapsible' => FALSE,
  );
  $mode_options['fb_scrape'] = t('Use custom settings');
  if (module_exists('fbconnect')) {
    $mode_options['fbconnect'] = t('Use Facebook Connect settings');
  }
  if (module_exists('fb_app')) {
    $fb_apps = fb_invoke(FB_OP_GET_ALL_APPS, NULL, array());
    foreach ($fb_apps as $delta => $fb_app) {
      if (!($fb_app->status > 0)) {
        unset($fb_apps[$delta]);
      }
    }
    if (count($fb_apps)) {
      $mode_options['fb_app'] = t('Use Facebook App settings');
      foreach ($fb_apps as $fb_app) {
        $fb_app_options[$fb_app->fba_id] = $fb_app->label;
      }
    }
  }
  $form['fb_scrape_app']['fb_scrape_mode'] = array(
    '#title' => t('Select Facebook App settings mode'),
    '#type' => 'radios',
    '#options' => $mode_options,
    '#description' => t('Setting to use for connect to Facebook App.'),
    '#required' => TRUE,
    '#default_value' => variable_get('fb_scrape_mode', NULL),
  );
  $form['fb_scrape_app']['fb_scrape_mode']['fb_scrape_app_id'] = array(
    '#title' => t('Application ID'),
    '#type' => 'textfield',
    '#weight' => 0.0011,
    '#default_value' => variable_get('fb_scrape_app_id', NULL),
    '#states' => array(
      'visible' => array(
        ':input[name="fb_scrape_mode"]' => array('value' => 'fb_scrape'),
      ),
      'required' => array(
        ':input[name="fb_scrape_mode"]' => array('value' => 'fb_scrape'),
      ),
    ),
  );
  $form['fb_scrape_app']['fb_scrape_mode']['fb_scrape_app_secret'] = array(
    '#title' => t('Application Secret'),
    '#type' => 'textfield',
    '#weight' => 0.0012,
    '#default_value' => variable_get('fb_scrape_app_secret', NULL),
    '#states' => array(
      'visible' => array(
        ':input[name="fb_scrape_mode"]' => array('value' => 'fb_scrape'),
      ),
      'required' => array(
        ':input[name="fb_scrape_mode"]' => array('value' => 'fb_scrape'),
      ),
    ),
  );
  if (count($fb_app_options)) {
    $form['fb_scrape_app']['fb_scrape_mode']['fb_scrape_fb_app_id'] = array(
      '#title' => t('Select Facebook App to use for scrape'),
      '#type' => 'radios',
      '#weight' => 0.0031,
      '#options' => $fb_app_options,
      '#default_value' => variable_get('fb_scrape_fb_app_id', NULL),
      '#states' => array(
        'visible' => array(
          ':input[name="fb_scrape_mode"]' => array('value' => 'fb_app'),
        ),
        'required' => array(
          ':input[name="fb_scrape_mode"]' => array('value' => 'fb_app'),
        ),
      ),
    );
  }
  $form['fb_scrape_type'] = array(
    '#type' => 'fieldset',
    '#title' => t('Type settings'),
    '#collapsible' => TRUE,
  );
  $entity_types = module_invoke_all('fb_scrape_entity_types');
  $form['fb_scrape_type']['fb_scrape_entity_type_enabled'] = array(
    '#title' => t('Enable for the following entity types'),
    '#type' => 'checkboxes',
    '#options' => $entity_types,
    '#description' => t('The entity types for which to enable Facebook scrape. If none are selected then scrape will be enabled for NONE content types.'),
    '#default_value' => variable_get('fb_scrape_entity_type_enabled', array()),
  );
  $node_types = node_type_get_types();
  $node_type_options = array();
  foreach ($node_types as $id => $data) {
    $node_type_options[$id] = $data->name;
  }
  $form['fb_scrape_type']['fb_scrape_node_type_enabled'] = array(
    '#title' => t('Enable for the following content types'),
    '#type' => 'checkboxes',
    '#options' => $node_type_options,
    '#description' => t('The content types for which to enable Facebook scrape. If none are selected then scrape will be enabled for ALL content types.'),
    '#default_value' => variable_get('fb_scrape_node_type_enabled', array()),
    '#states' => array(
      'visible' => array(
        ':input[name="fb_scrape_entity_type_enabled[node]"]' => array('checked' => TRUE),
      ),
    ),
  );
  return system_settings_form($form);
}

/**
 * Validation depend on selected mode.
 */
function fb_scrape_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['fb_scrape_mode'] == 'fb_scrape') {
    if ($form_state['values']['fb_scrape_app_id'] == '') {
      form_set_error('fb_scrape_app_id', t('The field %field is required.', array('%field' => t('Application ID'))));
    }
    if ($form_state['values']['fb_scrape_app_secret'] == '') {
      form_set_error('fb_scrape_app_secret', t('The field %field is required.', array('%field' => t('Application Secret'))));
    }
  }
  if ($form_state['values']['fb_scrape_mode'] == 'fb_app' && $form_state['values']['fb_scrape_fb_app_id'] == '') {
    form_set_error('fb_scrape_fb_app_id', t('The field %field is required.', array('%field' => t('Select Facebook App to use for scrape'))));
  }
}
