Force Facebook to update URL cache

-- INSTALLATION --
Facebook PHP SDK
PHP SDK v.4.x
@todo

PHP SDK v.3.x (deprecated)
Upload facebook-php-sdk (https://github.com/facebookarchive/facebook-php-sdk)
into the libraries folder.
So that it looks like: 'sites/all/libraries/facebook-php-sdk/src/facebook.php'

Supported entity types:
- node (with node type selection)
- user
- blog
- taxonomy_term
- forum

You can add another entity types with hook_fb_scrape_entity_types() function.

-- SETTINGS --
You have to configure Facebook Application connection.
There are 3 modes (2nd and 3rd depend on enabled modules):
1) Use custom settings
 - Create a new Facebook Application at: https://developers.facebook.com
 - Enter Application ID, Application Secret
2) Use Facebook Connect settings
 - https://www.drupal.org/project/fbconnect module settings
3) Use Facebook App settings
 - use Drupal for Facebook (https://www.drupal.org/project/fb) submodule:
   Facebook App (fb_app) settings

-- CUSTOM USING --
You can scrape your URLs by calling _fb_scrape_url($url) function.

-- MAINTENER --
Tamas Szanyi (https://www.drupal.org/user/389677) of Brainsum
(http://brainsum.com)
